﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AkamaLife;
public class ToUI : MonoBehaviour {
    
    [SerializeField] Image life, magic;
    [SerializeField] Text coins;
    [SerializeField] GameObject sword, axe, bow, magico;


    IthemsCounter counter;
    MineLifer lifer;
    NewMineTird controllerM;
	// Use this for initialization
	void Start () {
        counter = GetComponent<IthemsCounter>();
        lifer = GetComponent<MineLifer>();
        controllerM = GetComponent<NewMineTird>();
	}
	
	// Update is called once per frame
	void Update () {
        life.fillAmount = (1f * lifer.currentLife) / lifer.maxLife;
        magic.fillAmount = (1f * counter.magicPotions) / counter.magicPotionLimit;
        coins.text = counter.coins + "";
        sword.SetActive(controllerM.sword);
        axe.SetActive(controllerM.axe);
        bow.SetActive(controllerM.bow);
        magico.SetActive(controllerM.magic);
	}
}
