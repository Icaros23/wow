﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AkamaLife;
public class Item : MonoBehaviour {
    enum ItemClass{coin, healthPotion, magicPotion, axe, bow, sword, shield}
    [SerializeField] ItemClass tipo;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<IthemsCounter>()!= null)
        {
            switch(tipo)
            {
                case ItemClass.coin:
                    other.GetComponent<IthemsCounter>().coins+=1;
                    break;
                case ItemClass.healthPotion:
                    other.GetComponent<IthemsCounter>().healthPotions+=1;
                    break;
                case ItemClass.magicPotion:
                    other.GetComponent<MineLifer>().currentLife +=1;
                    break;
                case ItemClass.axe:
                    other.GetComponent<IthemsCounter>().axe = true;
                    break;
                case ItemClass.bow:
                    other.GetComponent<IthemsCounter>().bow = true;
                    break;
                case ItemClass.shield:
                    other.GetComponent<IthemsCounter>().shield = true;
                    break;
                case ItemClass.sword:
                    other.GetComponent<IthemsCounter>().sword = true;
                    break;
            }
            Destroy(gameObject);
        }
    }
}
